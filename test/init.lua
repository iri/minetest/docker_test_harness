local register_test = test_harness.get_test_registrator("test_harness", test_harness.version_string)
local get_node = minetest.get_node
local set_node = minetest.set_node
local air = "air"

-- How this works:
--   register_test registers a test with a name and function
--   The function should return if the test passes or otherwise cause a Lua error
--   The basic structure is: get areas + do operations + check results
-- Helpers:
--   area.get must be used to retrieve areas that can be operated on (these will be cleared before each test)
--   check.filled / check.not_filled can be used to check the result
--   area.margin + check.filled2 is useful to make sure nodes weren't placed too far
--   place_pattern + check.pattern is useful to test ops that operate on existing data


register_test("Internal self-test")

register_test("is area loaded?", function()
	local pos1, _ = area.get(1)
	assert(get_node(pos1).name == air)
end)

register_test("area.split", function()
	for i = 2, 6 do
		local pos1, pos2 = area.get(1, 1, i)
		local half1, half2 = area.split(pos1, pos2)
		assert(pos1.x == half1.x and pos1.y == half1.y)
		assert(half1.x == half2.x and half1.y == half2.y)
		assert(half1.z + 1 == half2.z)
		if i % 2 == 0 then
			assert((half1.z - pos1.z) == (pos2.z - half2.z)) -- divided equally
		end
	end
end)

register_test("check.filled", function()
	local pos1, pos2 = area.get(1, 2, 1)
	set_node(pos1, { name = testnode1 })
	set_node(pos2, { name = testnode2 })
	check.filled(pos1, pos1, testnode1)
	check.filled(pos1, pos2, { testnode1, testnode2 })
	check.not_filled(pos1, pos1, air)
	check.not_filled(pos1, pos2, { air, testnode3 })
end)

register_test("pattern", function()
	local pos1, pos2 = area.get(3, 2, 1)
	local pattern = { testnode1, testnode3 }
	place_pattern(pos1, pos2, pattern)
	assert(get_node(pos1).name == testnode1)
	check.pattern(pos1, pos2, pattern)
end)

for _, name in ipairs({
	"players",
}) do
	dofile(minetest.get_modpath("test_harness") .. "/test/" .. name .. ".lua")
end
