local register_test = test_harness.get_test_registrator("test_harness", test_harness.version_string)
local vec = vector.new

register_test("Tests utilities for players")

register_test("save_players", function()

    local player1 = minetest.get_player_by_name("player1")
    local itemstack = ItemStack("default:stone 30")
    player1:get_inventory():add_item("main", itemstack)

    local player_data = test_harness.save_players({"player1"})

    assert(player_data ~= nil)
    assert(player_data["player1"] ~= nil)
    assert(player_data["player1"].position ~= nil)
    assert(vector.distance(player_data["player1"].position,vec(0,0,0)) < 2)
    assert(player_data["player1"].privs ~= nil)
    assert(player_data["player1"].inventory ~= nil)
    assert(player_data["player1"].inventory.main ~= nil)
    assert(#player_data["player1"].inventory.main > 0)
    assert(player_data["player1"].inventory.main[1] ~= nil)
    assert(player_data["player1"].inventory.main[1]:get_name()  == "default:stone")
    assert(player_data["player1"].inventory.main[1]:get_count()  == 30)

end, { players={ "player1" },})


register_test("restore_players", function()


    local player_data = { ["player1"] = {
        position= vec(1,2,3),
        privs= { ["noclip"] = true,["interact"] = true,["debug"] = true,["fly"] = true,["basic_debug"] = true, ["shout"] = true },
        inventory= {
            ["craftpreview"] = { ItemStack("") },
            ["craftresult"] = { ItemStack(""),} ,
            ["main"] = { ItemStack("default:stone 30"), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""),} ,
            ["craft"] = {  ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""), ItemStack(""),} ,
        }
    } }
    local player1 = minetest.get_player_by_name("player1")

    assert(not (player1:get_inventory():contains_item("main", "default:stone")))
    assert(vector.distance(player1:get_pos(),vec(0,0,0)) < 2)
    assert(minetest.get_player_privs("player1")["fast"])
    assert(not minetest.get_player_privs("player1")["shout"])

    test_harness.restore_players(player_data)

    assert(player1:get_inventory():contains_item("main", "default:stone"))
    assert(player1:get_pos() == vec(1,2,3))
    assert(minetest.get_player_privs("player1")["shout"])
    assert(not minetest.get_player_privs("player1")["fast"])


end, { players={ "player1" },})