#!/bin/bash

export DISPLAY=:99

rm -f /tmp/.X1-lock

# Start Xvfb
# echo "Starting Xvfb"
# Xvfb $DISPLAY -ac -screen 0 "$XVFB_WHD" -nolisten tcp +extension GLX +render -noreset &
# xvfb_pid="$!"
# echo "Waiting for Xvfb (PID: $Xvfb_pid) to be ready..."
# while ! xdpyinfo -display "${DISPLAY}" > /dev/null 2>&1; do
#   sleep 0.1
# done
# echo "Xvfb is running."

# prevent spikes if you spawn many containers simultaneously
sleep $((2 + $RANDOM % 6))


if [ "${PLAYERNAME,,}" == "random" ]
then
  PLAYERNAME=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 8)
fi

if [ "$RANDOM_INPUT" == "1" ]
then
  xvfb-run minetest --address "$SERVER" --port "$PORT" --name "$PLAYERNAME" --password "$PASSWORD" --go --random-input &
else
  echo minetest --address "$SERVER" --port "$PORT" --name "$PLAYERNAME" --password "$PASSWORD" --go
  xvfb-run minetest --address "$SERVER" --port "$PORT" --name "$PLAYERNAME" --password "$PASSWORD" --go &
fi
minetest_pid=$!

function finish {
  if ps -p $minetest_pid > /dev/null ; then
    kill -n 9 $minetest_pid
    wait $minetest_pid
  fi
  # if ps -p $xvfb_pid > /dev/null ; then
  #   kill -n 9 $xvfb_pid
  #   wait $xvfb_pid
  # fi
  exit 0
}
trap finish EXIT TERM INT QUIT

wait $minetest_pid
