test_harness = {}
test_harness.modpath = minetest.get_modpath("test_harness")

local ver = { major = 0, minor = 1 }
test_harness.version = ver
test_harness.version_string = string.format("%d.%d", ver.major, ver.minor)


if minetest.settings:get_bool("test_harness_run_tests", false) then
	dofile(test_harness.modpath .. "/base.lua")
	dofile(test_harness.modpath .. "/test/init.lua")
end