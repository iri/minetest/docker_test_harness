# Docker Test Harness for Minetest Mods

## Overview

This mod provides an automated testing framework for Minetest mods by simulating player behavior on a Minetest server. It allows developers to create, register, and run tests to ensure the correct behavior of their mods in various scenarios.

## Features

- **Test Registration**: Easily register custom tests for your mod.
- **Player Behavior Simulation**: Simulate player actions for automatic testing.
- **Modular Structure**: Each test is associated with a specific mod and can be customized.
- **Area and Node Manipulation**: Test areas can be defined, and node checks can be performed.
- **Test Context and State Management**: Manage and track the progress of tests across multiple mods.

## Prerequisite

The tests are run using Docker or Podman. Therefore you'll need to have either one of them installed.
It also uses `bash`, `jq` and the gnu version of `mktemp` (found in the `coreutils` package for Alpine).
For automated testing, the mod is testing itself, so this repository shows an example of configuration
for the Gitlab CI. The main point being to have runners able to run docker pipelines and do docker in docker.

## Usage

This mod's goal is to provide a test framework for implementing tests for minetest mods.

1. Declare `test_harness` as an optional dependencies for your mod
2. Copy the `.util` folder in your project
3. Copy the `.util/.mod_env.json.tmpl` to `.mod_env.json` at the root of your folder.
4. Adapt its content. Do not remove the test_harness dependency definition.
5. The list of mods to test can be set in the `additional_mods` key. This list will be added to `current_mod`, if you add some mods, the list must start with a comma.
6. Customize the minetest server configuration by adding configuration values in the `minetest_add_conf` value. This must be a single string, with line returns if necessary.
7. You can add some file to the server's container with the `additional_files` value. This is an array of object having the `"path"` and `"content"` keys. If the content is in `json` then it is directly added as is in the resulting file.
8. If necessary, customize the `.util/Dockerfile` if the previous means are not sufficients.
9. In your source files (in the `init.lua` for example) add the following lines (or equivalent)

    ```lua
    if minetest.settings:get_bool("test_harness_run_tests", false) then
        dofile(minetest.get_modpath("my_mod").. "/test/init.lua")
    end
    ```

10. In your test file(s), get an instance of the method allowing the registration of your tests:

   ```lua
   local register_test = test_harness.get_test_registrator("my_mod", my_mod.version_string)
   ```

11. Register the test by calling `register_test`
12. Run the test by lauching the `run_tests.sh` script. For example with Podman

    ```shell
    $ .util/run_tests.sh --podman -c 1
    ```

    See the available options with `.util/run_tests.sh --help`

## Sources

- The base pf the code comes from the WorldEdit mod project : https://github.com/Uberi/Minetest-WorldEdit.
- The client's Dockerfile has been adapted from `Miney-pi` (https://github.com/miney-py/minetest-client-docker)
- The color management code comes from `lua-chroma`: https://github.com/ldrumm/lua-chroma

## License

Licensed under the AGPL (Affero General Public License).

## Funding

This mod is published with [funding from the NLNet Foundation](https://nlnet.nl/project/MinetestEdu/).
